FROM ubuntu:16.04

#MAINTAINER Wei-Ming Wu <wnameless@gmail.com>

MAINTAINER Cicito Sini <wotanf@gmail.com>
ADD assets /assets
RUN cat /assets/oracle-xe_11.2.0-1.0_amd64.deba* > /assets/oracle-xe_11.2.0-1.0_amd64.deb


# get rid of the message: "debconf: unable to initialize frontend: Dialog"
ENV DEBIAN_FRONTEND noninteractive

RUN /assets/prebuild.sh


#RUN apt-get update && \
#	apt-get install -y -q libapt-pkg5.0:amd64 libapt-inst2.0
# Prepare to install Oracle
#RUN dpkg -i assets/apt-utils_1.2.24_amd64.deb
RUN apt-get update && \
	apt-get install -y -q libaio1 bash net-tools bc curl rlwrap
	

RUN ln -s /usr/bin/awk /bin/awk && \
    mkdir /var/lock/subsys && \
    mv /assets/chkconfig /sbin/chkconfig && \
    chmod 755 /sbin/chkconfig && \
	dpkg --install /assets/oracle-xe_11.2.0-1.0_amd64.deb

		
RUN /assets/setup.sh

EXPOSE 22
EXPOSE 1521
EXPOSE 8080

VOLUME ["/home/oracle"]
VOLUME ["/u01/app/oracle/admin/XE/dpdump"]
#VOLUME ["/u01/app/oracle/oradata/XE"]

ENV processes 500
ENV sessions 555
ENV transactions 610




CMD /usr/sbin/startup.sh && /usr/sbin/sshd -D




